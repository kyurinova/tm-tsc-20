# TASK MANAGER

## DEVELOPER INFO

name: Kseniia Yurinova

e-mail: ks093050@mail.ru

e-mail: ks093050@yandex.ru

## HARDWARE

CPU: i5

RAM: 16G

SSD: 512GB

## SOFTWARE

System: Windows 10

Version JDK: 1.8.0_281

## APPLICATION BUILD

```bash
mvn clean install
```

## APPLICATION RUN

```bash
java -jar ./task-manager.jar
```
