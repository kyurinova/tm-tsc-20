package ru.tsc.kyurinova.tm;

import ru.tsc.kyurinova.tm.component.Bootstrap;

public class Application {

    public static void main(final String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start(args);
    }

}
