package ru.tsc.kyurinova.tm.exception.entity;

import ru.tsc.kyurinova.tm.exception.AbstractException;

public class TaskNotFoundException extends AbstractException {

    public TaskNotFoundException() {
        super("Error. Task not found.");
    }

}
