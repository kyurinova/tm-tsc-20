package ru.tsc.kyurinova.tm.service;

import ru.tsc.kyurinova.tm.api.repository.IProjectRepository;
import ru.tsc.kyurinova.tm.api.repository.ITaskRepository;
import ru.tsc.kyurinova.tm.api.service.IProjectTaskService;
import ru.tsc.kyurinova.tm.exception.empty.EmptyUserIdException;
import ru.tsc.kyurinova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kyurinova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kyurinova.tm.model.Project;
import ru.tsc.kyurinova.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final ITaskRepository taskRepository;

    private final IProjectRepository projectRepository;

    public ProjectTaskService(ITaskRepository taskRepository, IProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public Task bindTaskById(final String userId, final String projectId, final String taskId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        if (!taskRepository.existsById(userId, taskId)) throw new TaskNotFoundException();
        return taskRepository.bindTaskToProjectById(userId, projectId, taskId);
    }

    @Override
    public Task unbindTaskById(final String userId, final String projectId, final String taskId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        if (!taskRepository.existsById(userId, taskId)) throw new TaskNotFoundException();
        return taskRepository.unbindTaskToProjectById(userId, projectId, taskId);
    }

    @Override
    public void removeAllTaskByProjectId(final String userId, final String projectId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        taskRepository.removeAllTaskByProjectId(userId, projectId);
    }

    @Override
    public Project removeById(final String userId, final String projectId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        removeAllTaskByProjectId(userId, projectId);
        return projectRepository.removeById(userId, projectId);
    }

    @Override
    public List<Task> findAllTaskByProjectId(final String userId, String projectId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        return taskRepository.findAllTaskByProjectId(userId, projectId);
    }

}
